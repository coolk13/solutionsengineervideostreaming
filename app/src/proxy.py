import time
from jose import jwt
import json
import uuid

body = {"user": "username", "date": "todays date"}
key = "a9ddbcaba8c0ac1a0a812dc0c2f08514b23f2db0a68343cb8199ebb38a6d91e4ebfb378e22ad39c2d01d0b4ec9c34aa91056862ddace3fbbd6852ee60c36acbf"

def gen_token_header(x, y):
    """
    Generate the 'x-my-jwt' header for the POST request
    """
    now_payload = x["iat"] = int(time.time())
    gen_jti = uuid.uuid4().hex
    jti_id_nonce = x["jti"] = (gen_jti)
    jwt_token = jwt.encode(x, y, algorithm='HS512')
    return jwt_token

def request(flow):
    t = gen_token_header(body, key)
    flow.request.headers["x-my-jwt"] = str(t)
