HTTP_P = 9190
build:
	export HTTP_PORT=$(HTTP_P); \
	docker-compose build --no-cache

run:
	export HTTP_PORT=$(HTTP_P); \
	docker-compose up -d

stop:
	export HTTP_PORT=$(HTTP_P); \
	docker-compose down

cleanup:
	export HTTP_PORT=$(HTTP_P); \
	docker-compose down; \
	docker rmi proxy:latest; \
	docker rmi python:3.9.6-slim

test:
	docker run -t --rm proxy python -m unittest discover ./src