Usage:

Using postman or curl change proxy settings to Docker host's ip and $HTTP_PORT. 
<br>
Next POST at url https://postman-echo.com/post  'PROXYHOST:$HTTP_P'
<br>
See smuggled header "x-my-jwt" returned.

<br>

To build docker image:
> $ make build

To run unittests and remove a temp proxy container:
> $ make test

To start container and services for proxy, edit Makefile to choose bound port:
> $ make run

To stop container and services for proxy:
> $ make stop

To delete images, and labeled containers from build process
> $ make cleanup

<br>




Sequence Diagram:
[![](https://mermaid.ink/img/eyJjb2RlIjoic2VxdWVuY2VEaWFncmFtXG5wYXJ0aWNpcGFudCBDIGFzIENsaWVudFxucGFydGljaXBhbnQgUCBhcyBwcm94eS5weVxucGFydGljaXBhbnQgRCBhcyBFbmRwb2ludFxuYXV0b251bWJlclxuICAgIEMgLXggUDogUE9TVCByZXF1ZXN0IGZyb20gQ2xpZW50IHRvIEVuZHBvaW50XG4gICAgYWN0aXZhdGUgUFxuICAgICAgICBQIC0tPj4gUDogU2VuZCBDbGllbnQgSFRUUCB0cmFmZmljIHRvIEVuZHBvaW50XG4gICAgICAgIFAgLS0-PiBQOiBnZW5lcmF0ZSAneC1teS1qd3QnaGVhZGVyXG4gICAgICAgIFAgLT4-KyBEOiBwYXNzIHJlcXVlc3QgdG8gRW5kcG9pbnQgd2l0aCAneC1teS1qd3QnIGhlYWRlciBzbXVnZ2xlZFxuICAgIGRlYWN0aXZhdGUgUFxuICAgIGFjdGl2YXRlIERcbiAgICAgICAgRCAtLT4-IFA6IEVuZHBvaW50IHdlYnNlcnZlciByZXNwb25zZVxuICAgIGRlYWN0aXZhdGUgRFxuICAgIGFjdGl2YXRlIFBcbiAgICAgICAgUCAtLT4-IEM6IHBhc3N0aHJvdWdoIHJlc3BvbnNlIHBheWxvYWQgdG8gY2xpZW50ICdyZXNwb25zZV9wYXlsb2FkJ1xuICAgIGRlYWN0aXZhdGUgUCIsIm1lcm1haWQiOnsidGhlbWUiOiJkZWZhdWx0In0sInVwZGF0ZUVkaXRvciI6ZmFsc2UsImF1dG9TeW5jIjp0cnVlLCJ1cGRhdGVEaWFncmFtIjpmYWxzZX0)](https://mermaid-js.github.io/mermaid-live-editor/edit##eyJjb2RlIjoic2VxdWVuY2VEaWFncmFtXG5wYXJ0aWNpcGFudCBDIGFzIENsaWVudFxucGFydGljaXBhbnQgUCBhcyBwcm94eS5wXG5wYXJ0aWNpcGFudCBEIGFzIEVuZHBvaW50XG5hdXRvbnVtYmVyXG4gICAgQyAteCBQOiBQT1NUIHJlcXVlc3QgZnJvbSBDbGllbnQgdG8gRW5kcG9pbnRcbiAgICBhY3RpdmF0ZSBQXG4gICAgICAgIFAgLS0-PiBQOiBTZW5kIENsaWVudCBIVFRQIHRyYWZmaWMgdG8gRW5kcG9pbnRcbiAgICAgICAgUCAtLT4-IFA6IGdlbmVyYXRlICd4LW15LWp3dCdoZWFkZXJcbiAgICAgICAgUCAtPj4rIEQ6IHBhc3MgcmVxdWVzdCB0byBFbmRwb2ludCB3aXRoICd4LW15LWp3dCcgaGVhZGVyIHNtdWdnbGVkXG4gICAgZGVhY3RpdmF0ZSBQXG4gICAgYWN0aXZhdGUgRFxuICAgICAgICBEIC0tPj4gUDogRW5kcG9pbnQgd2Vic2VydmVyIHJlc3BvbnNlXG4gICAgZGVhY3RpdmF0ZSBEXG4gICAgYWN0aXZhdGUgUFxuICAgICAgICBQIC0tPj4gQzogcGFzc3Rocm91Z2ggcmVzcG9uc2UgcGF5bG9hZCB0byBjbGllbnQgJ3Jlc3BvbnNlX3BheWxvYWQnXG4gICAgZGVhY3RpdmF0ZSBQIiwibWVybWFpZCI6IntcbiAgXCJ0aGVtZVwiOiBcImRlZmF1bHRcIlxufSIsInVwZGF0ZUVkaXRvciI6ZmFsc2UsImF1dG9TeW5jIjp0cnVlLCJ1cGRhdGVEaWFncmFtIjpmYWxzZX0)