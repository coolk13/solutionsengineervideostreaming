import unittest
import time
from jose import jwt
import json
import uuid

# example proxy.py function
# def gen_token_header(x, y):
#    """
#    Generate the 'x-my-jwt' header for the POST request
#    """
#    now_payload = x["iat"] = int(time.time())
#    gen_jti = uuid.uuid4().hex
#    jti_id_nonce = x["jti"] = (gen_jti)
#    jwt_token = jwt.encode(x, y, algorithm='HS512')
#    return jwt_token

class TestProxy(unittest.TestCase):
    """
    Tests for proxy.py
    """
    def test_epoch(self):
        """
        Get an epoch time greater than 1970 but less then a distant future date
        """
        self.t = time.time()
        self.tfour = self.t + 4000
        self.assertGreater(int(self.t), 1566454995)
        self.assertLess(int(self.t), int(self.tfour))

    def gen_token_header(self, x, y):
        """
        Generate the 'x-my-jwt' header for the POST request
        """
        now_payload = x["iat"] = int(1626357247)
        gen_jti = "hex"
        jti_id_nonce = x["jti"] = (gen_jti)
        jwt_token = jwt.encode(x, y, algorithm='HS512')
        return jwt_token

    def test_jwt(self):
        """
        Test JWT generation
        """
        self.sign = 'secret'
        self.body = {"user": "username", "date": "todays date"}
        self.gen = self.gen_token_header(self.body, self.sign)
        self.decode = jwt.decode(self.gen, 'secret', algorithms=['HS512'])
        self.assertEqual(str(self.decode), "{'user': 'username', 'date': 'todays date', 'iat': 1626357247, 'jti': 'hex'}")

if __name__ == '__main__':  
    unittest.main()
